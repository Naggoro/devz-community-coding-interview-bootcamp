package arrays.palindrome;

public class Palindrome {

    public static boolean isValidString(String str){
        if (str==null || str.length()==0) return false;
        return true;
    }

    public static String cleanString(String str){
        char[] charArray = str.toCharArray();
        char[] specialCharArray = {' ', '!', '|', '"', '@', '·', '#', '$', '~', '%', '€', '&', '¬', '/', '(', ')', '=', '?', '\'',
                '¡', '¿', '`', '^', '[', ']', '*', '+', '´', 'ç', '¨', 'Ç', '{', '}', '-', '_', '.', ':', ',', ';'};
        int specialCharsSize = specialCharArray.length;
        int charArraySize = charArray.length;
        char[] tempCharArray = new char[charArraySize];
        int tempCharArrayIndex = 0;
        //Remove Special Characters
        for (int i=0; i<charArraySize; i++){
            boolean isNotSpecialChar = true;
            for(int j=0; j<specialCharsSize; j++){
                if(charArray[i] == specialCharArray[j]){
                    isNotSpecialChar = false;
                    break;
                }
            }
            if(isNotSpecialChar){
                tempCharArray[tempCharArrayIndex] = charArray[i];
                tempCharArrayIndex++;
            }
        }
        //Remove Whitespaces if existing
        if (tempCharArrayIndex != charArraySize) {
            char[] cleanCharArray = new char[tempCharArrayIndex];
            int cleanCharArrayIndex = 0;
            for (int i=0; i<tempCharArray.length; i++){
                if (tempCharArray[i] != 0) {
                    cleanCharArray[cleanCharArrayIndex] = tempCharArray[i];
                    cleanCharArrayIndex++;
                }
            }
            return String.valueOf(cleanCharArray).toLowerCase();
        }
        return String.valueOf(tempCharArray).toLowerCase();
    }

    public static boolean isPalindrome(String str){
        int start = 0;
        int end = str.length()-1;
        while (start<end) {
            if (str.charAt(start) != str.charAt(end)) return false;
            start++;
            end--;
        }
        return true;
    }
}
