package arrays.pascalTriangle;

public class PascalTriangle {

    public static int[] generateNthTriangleRow(int index){
        int[][] pascalTriangle = new int[index+1][index+1];
        for (int i=0; i<=index; i++){
            for (int j=0; j<=i; j++){
                if (j==0) pascalTriangle[i][j] = 1;
                else if (j!=i) pascalTriangle[i][j] = pascalTriangle[i-1][j-1]+pascalTriangle[i-1][j];
                else if (j==i){
                    pascalTriangle[i][j] = 1;
                    break;
                }
            }
        }
        return pascalTriangle[index];
    }
}
