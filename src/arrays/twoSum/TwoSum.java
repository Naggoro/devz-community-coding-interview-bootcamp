package arrays.twoSum;

public class TwoSum {

        public static int[] findSumIndexes(int[] numbers, int target){
        int[] indexes = new int[2];
        boolean isFound = false;
        for (int i=0; i<numbers.length && isFound == false; i++){
            for (int j=i+1; j<numbers.length && isFound == false; j++){
                if (numbers[i]+numbers[j] == target){
                    indexes[0] = i;
                    indexes[1] = j;
                    isFound = true;
                }
            }
        }
        return indexes;
    }
}
