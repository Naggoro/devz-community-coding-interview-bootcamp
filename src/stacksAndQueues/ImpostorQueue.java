package stacksAndQueues;

public class ImpostorQueue {
    Stack firstStack = new Stack();
    Stack secondStack = new Stack();

    public static void main(String[] args){
        ImpostorQueue queue = new ImpostorQueue();
        queue.push(1);
        queue.push(2);
        queue.push(3);
        System.out.println(queue.pop());
        System.out.println(queue.pop());
        System.out.println(queue.pop());
    }

    public int pop(){
        if (secondStack.isEmpty()) {
            while (!firstStack.isEmpty()) secondStack.push(firstStack.pop());
        }
        return secondStack.pop();
    }

    public void push(int value){
        firstStack.push(value);
    }

    public int peek(){
        if (secondStack.isEmpty()) {
            while (!firstStack.isEmpty()) secondStack.push(firstStack.pop());
        }
        return secondStack.peek();
    }

    public boolean isEmpty(){
        return firstStack.isEmpty() && secondStack.isEmpty();
    }
}