package stacksAndQueues;

public class Queue {
    Node head;
    Node tail;

    class Node {
        int data;
        Node next;

        public Node(int data){
            this.data = data;
        }
    }

    public static void main(String[] args){
        Queue queue = new Queue();
        queue.push(1);
        queue.push(2);
        queue.pop();
        queue.pop();
    }

    public void pop(){
        if (head != null && head.next == null) {
            head = null;
            tail = null;
        }
        else if (head != null && head.next != null) {
            head.data = head.next.data;
            head.next = head.next.next;
        }
    }

    public void push(int value){
        if (head == null) {
            head = new Node(value);
            tail = head;
        } else {
            tail.next = new Node(value);
            tail = tail.next;
        }
    }

    public int peek(){
        return head.data;
    }

    public boolean isEmpty(){
        return head == null;
    }
}
