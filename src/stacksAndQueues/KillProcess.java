package stacksAndQueues;

public class KillProcess {

    public static void main(String[] args){
        KillProcess kp = new KillProcess();

        //int[] ppid = {3,0,3,5};
        //int[] pid = {1,3,5,10};
        //int kill = 0;

        int[] ppid = {3,0,3,5,5,10};
        int[] pid = {1,3,5,10,4,2};
        int kill = 5;

        int[] result = kp.killPIDS(ppid, pid, kill);
        for (int i=0; i<result.length; i++){
            System.out.println(result[i]);
        }
    }

    //TODO: Move from arrays to data structures, lower time complexity
    public int[] killPIDS(int[] ppid, int[] pid, int kill){
        int PIDSNumber = pid.length;
        int[] killedPIDS = new int[PIDSNumber];
        int killIndex = 0;
        boolean killHasChildren = false;

        //If kill target has no parent, return whole PID
        for (int i=0; i<pid.length; i++){
            if (pid[i] == kill) {
                killIndex = i;
                if (ppid[killIndex] == 0) return pid;
                break;
            }
        }

        //If kill target is no parent, return only kill target
        for (int i=0; i<ppid.length; i++) {
            if (ppid[i] == kill) killHasChildren = true;
        }
        if (!killHasChildren) return new int[] {kill};

        //Find all kill target children
        int currentParent = kill;
        int killedPIDSIndex = 0;
        killedPIDS[killedPIDSIndex] = kill;
        killedPIDSIndex++;
        int k = 0;
        for (int i=0; i<PIDSNumber; i++) {
            //Find children for current parent
            for (int j=0; j<PIDSNumber; j++){
                if (ppid[j] == currentParent) {
                    killedPIDS[killedPIDSIndex] = pid[j];
                    killedPIDSIndex++;
                }
            }
            //No more children for current parent, if there is no next parent or everything has been killed then exit
            k++;
            if (k == killedPIDS.length || killedPIDS[k] == 0) break;
            else currentParent = killedPIDS[k];
        }
        //Trim zeroes from array
        int[] PIDS = new int[k];
        for (int i=0; i<killedPIDS.length; i++){
            if (killedPIDS[i] != 0) PIDS[i] = killedPIDS[i];
        }
        return PIDS;
    }
}
