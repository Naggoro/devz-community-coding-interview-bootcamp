package stacksAndQueues;

public class Stack {
    Node head;

    class Node {
        int data;
        int min;
        Node next;

        public Node(int data, int min){
            this.data = data;
            this.min = min;
        }
    }

    public static void main(String[] args) {
        Stack stack = new Stack();
        stack.push(3);
        stack.push(2);
        stack.push(1);
        System.out.println(stack.getMin());
        stack.pop();
        System.out.println(stack.getMin());


        String string1 = "())";
        //String string2 = "()))((";
        System.out.println(stack.parenthesisToMakeValid(string1));
    }

    public int pop() {
        int removedValue = head.data;
        head = head.next;
        return removedValue;
    }

    public void push(int value) {
        int min;
        if (head == null) head = new Node(value, value);
        else {
            if (value < head.min) min = value;
            else min = head.min;
            Node currentNode = head;
            head = new Node(value, min);
            head.next = currentNode;
        }
    }

    public int peek(){
        return head.data;
    }

    public boolean isEmpty() {
        return head == null;
    }

    public int getMin(){
        return head.min;
    }

    public int parenthesisToMakeValid(String parenthesis){
        java.util.Stack<Character> stack = new java.util.Stack();
        int length = parenthesis.length();
        int neededParenthesis = 0;
        int leftParenthesis = 0;
        for (int i=0; i<length; i++){
            char currentChar = parenthesis.charAt(i);
            if (currentChar == '(') stack.push(currentChar);
            else if (currentChar == ')' && stack.isEmpty()) neededParenthesis++;
            else if (currentChar == ')' && stack.pop() != '(') neededParenthesis++;
        }
        leftParenthesis = stack.size();
        return neededParenthesis+leftParenthesis;
    }
}