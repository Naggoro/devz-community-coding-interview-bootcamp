package treesAndGraphs;

import java.util.*;

public class Trie {
    TrieNode root;

    class TrieNode {
        boolean is_terminal;
        HashMap<Character, TrieNode> children;

        boolean IsValid(String s, int start_at) {
            if (s.length() == start_at) {
                return is_terminal;
            }
            char actual = s.charAt(start_at);
            if (!children.containsKey(actual)) {
                return false;
            }
            return IsValid(s, start_at + 1);
        }
    }

    public static void main(String[] args) {
        Trie dictionary = new Trie();
        LinkedList<String> strings = new LinkedList<>();
        strings.add("ted");
        strings.add("ten");
        strings.add("to");
        strings.add("inn");
        dictionary.build(strings);

        String testGetNext = "t";
        dictionary.getNext(testGetNext);

        String testGetAutocomplete = "t";
        dictionary.getAutocomplete(testGetAutocomplete);
    }

    //Create the dictionary with valid string. Build the Trie
    public void build(LinkedList<String> strings){
        Iterator<String> itr = strings.iterator();
        while (itr.hasNext()) {
            String currentString = itr.next();
            if (root == null)  root = new TrieNode();
            TrieNode currentRoot = root;
            for (int i=0; i<currentString.length(); i++) {
                char currentChar = currentString.charAt(i);
                if (currentRoot.children == null) currentRoot.children = new HashMap<>();
                if (!currentRoot.children.containsKey(currentChar)) currentRoot.children.put(currentChar, new TrieNode());
                currentRoot = currentRoot.children.get(currentChar);
            }
            currentRoot.is_terminal = true;
        }
    }

    //Return the possible valid characters after the input string.
    public LinkedList<Character> getNext(String input) {
        LinkedList<Character> possibleChars = new LinkedList<>();
        TrieNode currentRoot = root;
        //Traverse to node with the input last char
        for (int i=0; i<input.length(); i++) {
            char currentChar = input.charAt(i);
            if (currentRoot.children.containsKey(currentChar)) {
                currentRoot = currentRoot.children.get(currentChar);
            }
        }
        //If current node has children, add them to helperMap
        HashMap<Character, TrieNode> helperMap = new HashMap<>();
        if (!currentRoot.is_terminal) {
            Iterator itr = currentRoot.children.keySet().iterator();
            while (itr.hasNext()) {
                char c = (char) itr.next();
                helperMap.put(c, currentRoot.children.get(c));
                possibleChars.add(c);
            }
        }
        //For each node in helperMap find its children and store the char in a LinkedList.
        //Add current node's children to helperMap, then remove current node from helperMap.
        //Repeat until all children are checked ()
        int possibleCharsIndex = 0;
        while (!helperMap.isEmpty()) {
            char c = possibleChars.get(possibleCharsIndex);
            TrieNode currentChild = helperMap.get(c);
            if (!currentChild.is_terminal) {
                Iterator itr = currentChild.children.keySet().iterator();
                while (itr.hasNext()) {
                    char c2 = (char) itr.next();
                    helperMap.put(c2, currentChild.children.get(c2));
                    possibleChars.add(c2);
                }
            }
            helperMap.remove(c);
            possibleCharsIndex++;
        }
        return possibleChars;
    }

    //TODO: Return the list of valid string where the input string is a prefix.
    public LinkedList<String> getAutocomplete(String input) {
        LinkedList<String> possibleStrings = new LinkedList<>();
        TrieNode currentRoot = root;
        //Traverse to node with the input last char
        for (int i=0; i<input.length(); i++) {
            char currentChar = input.charAt(i);
            if (currentRoot.children.containsKey(currentChar)) {
                currentRoot = currentRoot.children.get(currentChar);
            }
        }
        return possibleStrings;
    }



}
