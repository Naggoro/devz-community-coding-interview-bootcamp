package treesAndGraphs;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class HR {
    Node root;

    class Node {
        int data;
        LinkedList<Node> children;

    }

    public static void main(String[] args) {
        HR hrSystem = new HR();
        //Map with EmployeeId - ManagerEmployeeId relation
        HashMap<Integer, Integer> employees = new HashMap<>();

        employees.put(1100, 1000);
        employees.put(1200, 1000);
        employees.put(1110, 1100);
        employees.put(1120, 1100);
        employees.put(1130, 1100);
        employees.put(1210, 1200);
        employees.put(1211, 1210);
        employees.put(1000, 0);
        hrSystem.build(employees);
        System.out.println("BYE");
    }

    //TODO: Create the dictionary with valid string. Build the Trie
    public void build(HashMap<Integer, Integer> employees){
        if (root == null) {
            root = new Node();
        }
        Node currentRoot = root;
        LinkedList<Integer> currentLevelChildren = new LinkedList<>();
        LinkedList<Integer> currentAddedChildren = new LinkedList<>();
        int currentManager = 0;
        while (!employees.isEmpty()) {
            boolean childrenAdded = false;
            for (Integer employee : employees.keySet()) {
                if (employees.get(employee) == 0) {
                    currentRoot.data = employee;
                    currentRoot.children = new LinkedList<>();
                    currentAddedChildren.add(employee);
                    //employees.remove(employee);
                    childrenAdded = true;
                    currentManager = employee;
                    break;
                } else if (employees.get(employee) == currentManager) {
                    Node tempNode = new Node();
                    tempNode.data = employee;
                    tempNode.children = new LinkedList<>();
                    currentRoot.children.add(tempNode);
                    currentLevelChildren.add(employee);
                    currentAddedChildren.add(employee);
                    //employees.remove(employee);
                    childrenAdded = true;
                }
            }
            for (Integer addedEmployee : currentAddedChildren) {
                employees.remove(addedEmployee);
            }
            if (!childrenAdded) {
                currentManager = currentLevelChildren.getFirst();
                for (Node child : currentRoot.children) {
                    if (child.data == currentManager) {
                        currentRoot = child;
                    }
                }
                currentLevelChildren.remove(currentManager);
            }
        }
    }
}
