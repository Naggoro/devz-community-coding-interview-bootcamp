package linkedLists.impl;

import java.util.HashSet;

public class LinkedList {
    Node head;

    class Node {
        int data;
        Node next;

        public Node(int data){
            this.data = data;
        }
    }

    public static void main(String[] args){
        LinkedList testList = new LinkedList();
        testList.testRemoveNodeWithinList();
    }

    public boolean isEmpty(){
        return head == null ? true : false;
    }

    public void add(int data){
        if (isEmpty()) {
            head = new Node(data);
        }
        else {
            Node currentNode = head;
            while (currentNode.next != null){
                currentNode = currentNode.next;
            }
            currentNode.next = new Node(data);
        }
    }

    public void add(int index, int data){
        Node currentNode = head;
        Node previousNode;
        Node newNode = new Node(data);
        if ((isEmpty() && index>0) || (index > getLastIndex()+1)) throw new IndexOutOfBoundsException();
        else if (currentNode == null) currentNode = newNode;
        else if (index == 0 && currentNode != null) {
            head = newNode;
            head.next = currentNode;
        }
        for (int i=1; i<=index; i++) {
            previousNode = currentNode;
            currentNode = currentNode.next;
            if (i==index){
                previousNode.next = newNode;
                previousNode.next.next = currentNode;
            }
        }
    }

    public boolean removeFirst(int data){
        Node currentNode = head;
        Node previousNode;
        if (currentNode != null && currentNode.data == data) {
            head = head.next;
            return true;
        }
        while (currentNode.next != null){
            previousNode = currentNode;
            currentNode = currentNode.next;
            if (currentNode.data == data){
                previousNode.next = currentNode.next;
                return true;
            }
        }
        return false;
    }

    public int removeByIndex(int index){
        Node currentNode = head;
        Node previousNode;
        int removedData = 0;
        if (index>getLastIndex()) throw new IndexOutOfBoundsException();
        if (index == 0) {
            removedData = head.data;
            head = head.next;
            return removedData;
        }
        for (int i=1; i<=index; i++) {
            previousNode = currentNode;
            currentNode = currentNode.next;
            if (i==index){
                removedData = currentNode.data;
                previousNode.next = currentNode.next;
            }
        }
        return removedData;
    }

    public int getLastIndex(){
        int lastIndex = 0;
        Node currentNode = head;
        if (currentNode == null) throw new NullPointerException();
        while (currentNode.next != null){
            lastIndex++;
            currentNode = currentNode.next;
        }
        return lastIndex;
    }

    public void printAll(){
        Node currentNode = head;
        while (currentNode != null){
            System.out.println(currentNode.data);
            currentNode = currentNode.next;
        }
    }

    //TODO: Add new version with usage of a Set
    public void removeDuplicates(){
        Node currentNode = head;
        Node evaluatedNode;
        Node previousNode;
        while (currentNode != null){
            previousNode = currentNode;
            evaluatedNode = currentNode.next;
            while (evaluatedNode != null){
                if (currentNode.data == evaluatedNode.data) previousNode.next = evaluatedNode.next;
                else previousNode = evaluatedNode;
                evaluatedNode = evaluatedNode.next;
            }
            currentNode = currentNode.next;
        }
    }

    //TODO: Replace usage of add method and implementation here
    public LinkedList sumTwoLinkedLists(LinkedList list1, LinkedList list2){
        LinkedList result = new LinkedList();
        Node currentNode1 = list1.head;
        Node currentNode2 = list2.head;
        int currentNumberList1;
        int currentNumberList2;
        int tempSum;
        int carryOver = 0;
        while (currentNode1 != null || currentNode2 != null){
            currentNumberList1 = 0;
            currentNumberList2 = 0;
            if (currentNode1 != null){
                currentNumberList1 = currentNode1.data;
                currentNode1 = currentNode1.next;
            }
            if (currentNode2 != null){
                currentNumberList2 = currentNode2.data;
                currentNode2 = currentNode2.next;
            }
            tempSum = currentNumberList1 + currentNumberList2 + carryOver;
            if (tempSum > 9) {
                carryOver = tempSum/10;
                result.add((tempSum-(carryOver*10)));
            }
            else result.add(tempSum);
        }
        if (carryOver > 0) result.add(carryOver);
        return result;
    }

    public Node getKNode(LinkedList list, int k){
        int size = list.getLastIndex();
        Node currentNode = list.head;
        Node kNode = new Node(0);
        for (int i=0; i<=size-k; i++){
            kNode = currentNode;
            currentNode = currentNode.next;
        }
        return kNode;
    }

    public Node getKNodeV2(LinkedList list, int k){
        Node currentNode = list.head;
        Node kNode = currentNode;
        int currentIndex = 0;
        if (list.head == null) throw new NullPointerException();
        while (currentNode.next != null){
            currentNode = currentNode.next;
            if (currentIndex != k) currentIndex++;
            else if (currentNode != null) kNode = kNode.next;
        }
        if (currentIndex != k) throw new IndexOutOfBoundsException();
        else return kNode;
    }

    public boolean hasCircularReference(LinkedList list){
        Node currentNode = list.head;
        HashSet<Node> hs = new HashSet<>();
        boolean hasCircularReference = false;
        while (currentNode != null && hasCircularReference == false) {
            hasCircularReference = !hs.add(currentNode);
            currentNode = currentNode.next;
        }
        return hasCircularReference;
    }

    public void removeNodeWithinList(Node nodeToRemove){
        while (nodeToRemove.next != null){
            nodeToRemove.data = nodeToRemove.next.data;
            if (nodeToRemove.next.next != null) nodeToRemove = nodeToRemove.next;
            else nodeToRemove.next = null;
        }
    }

    public void removeNodeWithinListConstantTime(Node nodeToRemove){
        nodeToRemove.data = nodeToRemove.next.data;
        nodeToRemove.next = nodeToRemove.next.next;
    }

    public void testRemoveNodeWithinList(){
        LinkedList list = new LinkedList(); //0 1 2 3 4 5
        list.head = new Node(0);
        Node currentNode = list.head;
        for (int i=1; i<=5; i++){
            currentNode.next = new Node(i);
            currentNode = currentNode.next;
        }
        Node nodeToRemove = list.head.next.next; //Nodo con valor 2

        list.printAll();
        list.removeNodeWithinList(nodeToRemove);
        System.out.println("---");
        list.printAll();
        System.out.println("---");

        LinkedList list2 = new LinkedList(); //0 1 2 3 4 5
        list2.head = new Node(0);
        Node currentNode2 = list2.head;
        for (int i=1; i<=5; i++){
            currentNode2.next = new Node(i);
            currentNode2 = currentNode2.next;
        }
        Node nodeToRemove2 = list2.head.next.next; //Nodo con valor 2

        list2.printAll();
        list2.removeNodeWithinList(nodeToRemove2);
        System.out.println("---");
        list2.printAll();
        System.out.println("---");

    }

}
